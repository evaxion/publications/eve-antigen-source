
# Endogenous viral elements constitute a complementary source of antigens for personalized cancer immunotherapy 

This repository contains sample code to conduct the computational analyses in the manuscript 'Endogenous viral elements constitute a complementary source of antigens for personalized cancer immunotherapy'. 

## Installation

Run `make install` to install a virtual environment and install the packages.

The workflows use a number of public tools. Singularity containers are built by 
navigating to the 'tools' directory and runing 'build.sh'

```bash
cd tools
bash build.sh
```

## Genomic reference genomes

Navigate to the 'references' directory and run the snakemake workflow to download and index the reference genomes.

```bash
source virt/bin/activate
cd workflows/references
snakemake -j 32 --use-singularity --singularity-args "--bind ${PWD} -u" 
```

## Antigen burden workflows

Two example workflows are provided in the 'workflows' directory.

The EVE antigen burden workflow is run by
 
```bash
source virt/bin/activate
cd workflows/eve_burden

sm_dir=$(dirname $(dirname $PWD))
snakemake -j 32 --use-singularity --singularity-args "--bind ${sm_dir} -u" 
```

The tumor mutational burden workflow is run by
```bash
source virt/bin/activate
cd workflows/variant_calling

sm_dir=$(dirname $(dirname $PWD))
snakemake -j 32 --use-singularity --singularity-args "--bind ${sm_dir} -u" 
```

#!/bin/bash

set -e

## Install SRATOOLS

SRATOOL_VER=3.0.2

mkdir -p /build; cd /build
wget --output-document sratoolkit.tar.gz https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/${SRATOOL_VER}/sratoolkit.${SRATOOL_VER}-ubuntu64.tar.gz
tar -xzf sratoolkit.tar.gz

mv sratoolkit.${SRATOOL_VER}-ubuntu64/bin/* /exg/bin

cd && rm -rf /build


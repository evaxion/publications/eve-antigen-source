#!/bin/bash

set -e

## Install dependencies
DEBIAN_FRONTEND="noninteractive" apt install -y tzdata
apt install -y wget make g++ gcc zlib1g-dev libbz2-dev liblzma-dev
apt install -y perl python3 r-base

## Install RSEM
RSEM_VER=1.3.3

mkdir -p /build; cd /build
wget https://github.com/deweylab/RSEM/archive/refs/tags/v${RSEM_VER}.tar.gz
tar xzf v${RSEM_VER}.tar.gz
cd RSEM-${RSEM_VER}

make install prefix=/exg

#rm v${RSEM_VER}.tar.gz
#ln -s /tools/RSEM-${RSEM_VER}/rsem-* /exg/bin


cd && rm -rf /build


#!/bin/bash

set -e

## Dependencies
apt install -y wget make gcc zlib1g-dev libbz2-dev liblzma-dev

## Install samtools
BWA_VER=0.7.17

mkdir -p /build; cd /build

wget https://github.com/lh3/bwa/releases/download/v${BWA_VER}/bwa-${BWA_VER}.tar.bz2
tar -xf bwa-${BWA_VER}.tar.bz2
cd bwa-${BWA_VER}
make
mv bwa /exg/bin

cd && rm -rf /build

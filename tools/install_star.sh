#!/bin/bash

set -e

## Install STAR
STAR_VER=2.7.9a

mkdir -p /build; cd /build
wget https://github.com/alexdobin/STAR/archive/refs/tags/${STAR_VER}.tar.gz
tar xzf ${STAR_VER}.tar.gz
mv STAR-${STAR_VER}/bin/Linux_x86_64_static/STAR* /exg/bin

cd && rm -rf /build


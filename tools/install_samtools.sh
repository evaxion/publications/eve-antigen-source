#!/bin/bash

set -e

## Dependencies
apt install -y wget make gcc zlib1g-dev libbz2-dev liblzma-dev

## Install samtools
SAMTOOLS_VER=1.13

mkdir -p /build; cd /build
wget https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VER}/samtools-${SAMTOOLS_VER}.tar.bz2
tar jxf samtools-${SAMTOOLS_VER}.tar.bz2
cd samtools-${SAMTOOLS_VER}
./configure --prefix=/exg --without-curses
make -j $(nproc --all)
make install
cd && rm -rf /build

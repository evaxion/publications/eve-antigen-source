FROM bioconductor/bioconductor_docker:RELEASE_3_15

RUN R --slave -e 'BiocManager::install("WGCNA")'
RUN R --slave -e 'devtools::install_version("WGCNA", version = "1.71")'

ENTRYPOINT R

